#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
  bool is_valid;
  char message[1000];
  char infile[100];
  char outfile[100];
  bool custom_key;
  char keyfile[100];
  bool is_interactive;
} Cli;

char* get_help_msg(){
  return "Usage: ./cipher [input.txt] [output.txt] [--key|-k:optional] key";
}
char* get_interactive_mode_welcome_msg(){
  return "Welcome to interactive Mode!";
}


Cli parse_cli_args(int argc, char* argv[]){
  Cli cli = {};

  // Assure valid generally and set it to false in some conditions
  cli.is_valid = true;

  // No of arguments provided (argc is always 1 since first one is the name of our program itself)
  int arguments = argc - 1;

  // No arguments provided
  if (argc == 0){
    cli.is_valid = false;
    return cli;
  }

  // Single argument provided
  if (argc == 1){
    // if user selected --interactive flag 
    if (strcmp(argv[1], "--interactive") == 0 || strcmp(argv[1], "-i") == 0){
      cli.is_interactive = true;
      cli.message = get_interactive_mode_welcome_msg();
      return cli;
    }else{
      // if user selected any other stuff
      cli.is_valid = false;
      return cli;
    }
  }

  // two argument provided assume them to be input and output files
  if (argc == 2){
    strcpy(cli.infile, argv[1]);
    strcpy(cli.outfile, argv[2]);
    cli.is_valid = true;
    return cli;
  }
   
  return cli;
}

int main(int argc, char* argv[]){
  printf(":: Received %d arguments. %s | %s | %s\n", argc, argv[1], argv[2], argv[3]);
  printf(":: %s is reading input file '%s'....\n", argv[0], argv[1]);
  return 0;
}
