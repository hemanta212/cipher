#include <stdio.h>

int main(int argc, char* argv[]){
  printf(":: Received %d arguments. %s | %s | %s\n", argc, argv[1], argv[2], argv[3]);
  printf(":: %s is reading input file '%s'....\n", argv[0], argv[1]);
  return 0;
}
